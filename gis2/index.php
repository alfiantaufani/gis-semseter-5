<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GIS 5</title>
    <script src="https://use.fontawesome.com/bca161a0fd.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css" type="text/css"/>


    <style>
        .full-width{
            width: 100%; 
            position : absolute;
            left : 0;
            top: 0;
            bottom : 0;
        }
        .a{
            position : fixed;
            margin : 20px;
        }
    </style>


</head>
<body>

    
    <div id="petaku" class="full-width"></div>





    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="data.js"></script>
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiYWxmaWFudGF1ZmFuaSIsImEiOiJja2cyY2oxYTgwODExMnNxd3Iyc3ZuYzlxIn0.IOR4ClWmnwgYUW1VY5Gngw';
        var petaku = new mapboxgl.Map({
            container: 'petaku',
            style: 'mapbox://styles/mapbox/satellite-v9',
            //style: 'mapbox://styles/mapbox/streets-v11',
            center : [112.2320841, -7.5197630],//lokasi kampus
            zoom : 16,
        });

        function caridata(query) {
            var datacocok = [];
            for (var i = 0; i < dataku.kumpulan.length; i++) {
                var x = dataku.kumpulan[i]; 
                    if (x.properties.judul.toLowerCase().search(query.toLowerCase()) !== -1) {
                    x['place_name'] = x.properties.judul;
                    x['center'] = x.geometry.coordinates;
                    x['place_type'] = ['park'];
                    datacocok.push(x);
                }
            }
            return datacocok;
        }

        petaku.addControl(
            new MapboxGeocoder({
                accessToken: mapboxgl.accessToken,
                localGeocoder : caridata,
                zoom : 17,
                mapboxgl: mapboxgl
            })
        );
        

    </script>
    
</body>
</html>