
<!DOCTYPE html>
<html>
    <head>
        <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
        <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
        <script src='jquery-3.4.1.min.js'></script>
        <style>
            .full-layar {
                width: 100%;
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
            }
            .layout {
	           border: none;
	           color: black;
	           padding: 10px;
	           text-align: center;
	           display: inline-block;
	           font-size: 14px;
	           position: fixed;
	           right: 20px;
	           border-radius: 8px;
			}
        </style>
    </head>
    <body>
        <div id='blokpeta' class="full-layar"></div>
        <select style="top: 10px; width: 300px" class="layout" id="pilih"></select>
        <input type="text" style="top: 60px; width: 275px" class="layout" id="radius" placeholder="isi Radius dalam Km">
        <button type="button" class="layout" onclick="hitung()" style="background-color: #2196f3; top: 110px; color: white;" id="hitung">Munculkan Lokasi Dalam Radius</button>
        <button type="button" class="layout"  onclick="reset()" style="background-color: #f44336; top: 110px; color: white; display: none;" id="reset">Reset</button>
        
        <script>
            mapboxgl.accessToken = 'pk.eyJ1IjoienVrZWVybmFuZGEiLCJhIjoiY2tmeWx0cDJuMmNiNjJ4bzhjbXNjM2NtZiJ9.uH4hshhusCg5AkBg_KgCLA';
            var petaku = new mapboxgl.Map({
                container: 'blokpeta',
                style: 'mapbox://styles/mapbox/satellite-v9',
                center: [112.23344889101912, -7.55676746650353],
                zoom: 15
            });
            let mapMarkers = [];
            option();
            function option() {
                var hasil = "";
                $.getJSON("json.php",{fungsi :"data_lokasi"}, function(result) {
                    if (result.length != 0) {
                        $.each(result, function (i, kolom) {
                            var lokasi = kolom.Lokasi;
                            var lintang = kolom.Lintang;
                            var bujur = kolom.Bujur;
                            var koordinat = bujur + "|" + lintang;
                            hasil += "<option value='" + koordinat + "'>" + lokasi + "</option>"; 
                        });
                        $("#pilih").html(hasil);
                    }
                });
            }
            function hitung() {
                var awal = $("#pilih").val();
                var jarak = $("#radius").val();
                var a= awal.split("|");
                var bujur= a[0];
                var lintang= a[1];
                var hasil = "";
                $.getJSON("json.php",{fungsi :"data_radius", bujur : bujur, lintang: lintang, jarak: jarak}, function(result) {
                    if (result.length != 0) {
                        $.each(result, function (i, kolom) {
                            var lokasi = kolom.Lokasi;
                            var lintang = kolom.Lintang;
                            var bujur = kolom.Bujur;
                            var jarak = kolom.distance;
                            var coba = '<div class="card">\
                                            <div class="card-header alert-danger text-center p-1">\
                                            <strong>'+ lokasi +' </strong><br>Jarak : '+ jarak.substr(0,4) +' Km\
                                            </div>\
                                            <div class="card-body p-0">\
                                            \
                                            </div>\
                                        </div>  \
                                        ';
                            var pop = new mapboxgl.Popup({offset: [0, -15]}).setHTML(coba);
                            const marker = new mapboxgl.Marker().setLngLat([bujur, lintang]).setPopup(pop).addTo(petaku);
                            mapMarkers.push(marker);
                            marker.togglePopup();
                        });
                    }
                    $("#hitung").css('display','none');
                    $("#reset").css('display','');
                })
                
            }
            function reset() {
                mapMarkers.forEach((marker) => marker.remove());
                mapMarkers = [];
                $("#hitung").css('display','');
                $("#reset").css('display','none');
            }
        </script>
    </body>
</html>