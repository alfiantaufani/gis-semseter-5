-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2021 at 12:32 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbuasgis`
--

-- --------------------------------------------------------

--
-- Table structure for table `lokasi2`
--

CREATE TABLE `lokasi2` (
  `ID` char(5) NOT NULL,
  `Lokasi` text NOT NULL,
  `Lintang` varchar(25) NOT NULL,
  `Bujur` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lokasi2`
--

INSERT INTO `lokasi2` (`ID`, `Lokasi`, `Lintang`, `Bujur`) VALUES
('01', 'Alun-Alun Jombang', '-7.556873823056748', '112.23344889101952'),
('02', 'Stasiun Jombang', '-7.558307314377913', '112.23316621126406'),
('03', 'Kebon Rojo', '-7.554022647057337', '112.23491699221034'),
('04', 'RSUD Jombang', '-7.549583133973513', '112.23645622918468'),
('05', 'GOR Stikes Pemkab', '-7.550174633057807', '112.22857568391993'),
('06', 'GOR Pemkab', '-7.538937322649801', '112.2467425079115'),
('07', 'RSIA Muslimat', '-7.543728207143786', '112.23382291416465'),
('08', 'TMP Kab. Jombang', '-7.544496089217958', '112.23127674498238'),
('09', 'Klenteng Jombang', '-7.540837889448198', '112.23190353226295'),
('10', 'UNWAHA Jombang', '-7.5201930194191675', '112.23228245209668'),
('11', 'Pengadilan Agama Jombang', '-7.542860510006136', '112.21800337295724'),
('12', 'Samsat Jombang', '-7.531173012540435', '112.24694873955036');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lokasi2`
--
ALTER TABLE `lokasi2`
  ADD PRIMARY KEY (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;