-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Jan 2021 pada 08.48
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_datagis`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('admin','user','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_lokasi`
--

CREATE TABLE `detail_lokasi` (
  `id` int(10) NOT NULL,
  `id_lokasi` varchar(10) NOT NULL,
  `lintang` varchar(50) NOT NULL,
  `bujur` varchar(50) NOT NULL,
  `kasus_positif` varchar(50) NOT NULL,
  `kasus_sembuh` varchar(50) NOT NULL,
  `kasus_meninggal` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_lokasi`
--

INSERT INTO `detail_lokasi` (`id`, `id_lokasi`, `lintang`, `bujur`, `kasus_positif`, `kasus_sembuh`, `kasus_meninggal`) VALUES
(1, '1', '-7.538729016395507', '112.23756876406605', '20', '10', '20'),
(2, '2', '-7.538449372542914', '112.27777987212829', '1', '10', '20'),
(3, '3', '-7.590813994919685', '112.23714575289966', '1', '18', '38'),
(4, '4', '-7.570674507880668', '112.34330238943755', '30', '33', '23'),
(5, '5', '-7.64115858004277', '112.29708107879895', '30', '22', '13'),
(6, '6', '-7.518307444254532', '112.34025483049362', '33', '24', '12'),
(7, '7', '-7.6935106531518755', '112.27117682778169', '34', '31', '45'),
(8, '8', '-7.580866124012815', '112.15707182605371', '45', '12', '10'),
(9, '9', '-7.478934229367539', '112.23216338511645', '10', '10', '10'),
(10, '10', '-7.46572512905162', '112.33701008736324', '30', '13', '14'),
(11, '11', '-7.454580546062047', '-7.454580546062047', '24', '22', '43'),
(12, '12', '-7.62688418860526', '112.19036116614905', '20', '23', '10'),
(13, '13', '-7.491778356007288', '112.16925814550484', '20', '23', '21'),
(14, '14', '-7.684981151114769', '112.29734175692579', '20', '29', '28'),
(15, '15', '-7.594194227363019', '112.12340096485326', '20', '22', '30'),
(18, '16', '-7.398446627994048', '112.21598162222921', '12', '11', '10'),
(19, '17', '-7.467195154822036', '112.18334033555874', '20', '10', '19'),
(20, '18', '-7.419465554664228', '112.33268884161316', '20', '21', '30'),
(21, '19', '-7.430516931918163', '112.30937479975773', '20', '19', '20'),
(22, '20', '-7.7106795683663165', '112.37053435968483', '30', '20', '10'),
(23, '21', '-7.591558847250269', '112.26780286830274', '20', '10', '10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lokasi`
--

INSERT INTO `lokasi` (`id`, `nama`) VALUES
(1, 'Jombang'),
(2, 'Peterongan'),
(3, 'Diwek'),
(4, 'Mojoagung'),
(5, 'Mojowarno'),
(6, 'Sumobito'),
(7, 'Ngoro'),
(8, 'Perak'),
(9, 'Tembelang'),
(10, 'Kesamben'),
(11, 'Ploso'),
(12, 'Gudo'),
(13, 'Megaluh'),
(14, 'Bareng'),
(15, 'Bandar Kedung Mulyo'),
(16, 'Kabuh'),
(17, 'Plandaan'),
(18, 'Ngusikan'),
(19, 'Kudu'),
(20, 'Wonosalam'),
(21, 'Jogoroto');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `detail_lokasi`
--
ALTER TABLE `detail_lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `detail_lokasi`
--
ALTER TABLE `detail_lokasi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
