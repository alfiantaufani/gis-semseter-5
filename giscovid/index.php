<?php
error_reporting(0);
include 'database.php';
session_start();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Menampilkan data json menjadi peta dalam kasus data COVID-19 dengan sumber dari https://api.kawalcorona.com">
  <meta name="author" content="unsorry">

  <link href="" rel="shortcut icon" type="image/png">
  <title>GIS - Covid19</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/app.css">
  <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
  <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>

  <!-- gl-direction -->
  <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js"></script>
  <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.css" type="text/css" />
  

  <style type="text/css">
    .full-layar {
                width: 100%;
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
            }
    .marker {
      background-image: url('https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png');
      background-size: cover;
      width: 50px;
      height: 50px;
      border-radius: 50%;
      cursor: pointer;
    }
  </style>

</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="index.php"><i class="fas fa-map-marker-alt"></i> <b>GIS Covid19 Kab. Jombang</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a href="#" class="nav-link" onclick="jenispeta('satellite-v9')"><i class="fas fa-map"></i></i> Peta Satelit</a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link" onclick="jenispeta('streets-v11')"><i class="fas fa-map"></i></i> Peta Street</a>
        </li>
        <?php
          error_reporting(0);
          include 'database.php';
          session_start();
          if($_SESSION['status'] != "login"){
            echo '<li class="nav-item" style="margin-left: 10px;">
            <a class="btn btn-outline-info" href="#" data-toggle="modal" data-target="#login"><i class="fas fa-lock"></i> Login</a>
          </li>';
          }else{
            echo '<li class="nav-item">
                    <a class="btn btn-info" href="#" data-toggle="modal" data-target="#infoModal"><i class="fas fa-plus-circle"></i> Input Kasus</a>
                  </li>
                  <li class="nav-item" style="margin-left: 10px;">
                    <a class="btn btn-outline-info logout" href="#"><i class="fas fa-lock"></i> Logout</a>
                  </li>';
          }
        ?>

  
        
        
      </ul>
    </div>
  </nav>
  <!-- Modal -->
  <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-dark text-light">
          <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-plus-circle"></i> Input Kasus</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color: #fff;">&times;</span>
          </button>
        </div>
        
        <div class="modal-body">
          <form method="post" id="form">
            <div class="form-group">
              <label for="exampleInputEmail1">Kecamatan</label>
              <select name="kecamatan" id="kecamatan" class="form-control" required="">
                <option disabled selected hidden="">Pilih Kecamatan</option>
                <?php 
                  include 'database.php';
                  $sql= "SELECT * FROM lokasi";
                  $hasil=mysqli_query($koneksi,$sql);
                  while ($data = mysqli_fetch_array($hasil)) {
                ?>
                <option value="<?php echo $data["id"]; ?>"><?php echo $data["nama"]; ?></option>
                <?php } ?>
              </select>
              <div id="cek"></div>
            </div>
            <div id="tampil">
              
            </div>
            <input type="submit" name="submit" class="btn btn-info btn-block" onclick="return confirm('Apakah Data Sudah Benar?')" value="Simpan">
          </form>
        </div>        
      </div>
    </div>
  </div>

  <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-dark text-light">
          <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-lock"></i> Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color: #fff;">&times;</span>
          </button>
        </div>
        
        <div class="modal-body">
          <form action="proses_login.php" method="post" id="form">
            <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text" class="form-control" name="username" placeholder="Masukkan Username">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Password</label>
              <input type="password" class="form-control" name="password" placeholder="Masukkan Password">
            </div>
            <input type="submit" name="login" class="btn btn-info btn-block" value="LOGIN">
          </form>
        </div>        
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <!-- map -->
    <div class="row row-map">
      <div class="col-sm">
        <div id="blokpeta" class="full-layar"></div>
      </div>
    </div>

    <div class="row row-info">
      <?php
        $dataIndonesia = file_get_contents("https://api.kawalcorona.com/indonesia/");
        $kasusIndonesia = json_decode($dataIndonesia);

        foreach($kasusIndonesia as $item){
      ?>
      <div class="col-sm-12 text-center text-light bg-dark">
        <div class="row p-3">
          <div class="col text-center">
            <h5><strong>Data Kasus Di Indonesia</strong></h5>
          </div>
        </div>
      </div>
      <div class="col-sm-4 text-center text-warning bg-dark">
        <div class="row p-5">
          <div class="col-3">
            <i class="far fa-sad-tear fa-4x"></i>
          </div>
          <div class="col text-left">
            <h5><strong>TOTAL POSITIF</strong></h5>
            <h5><?php echo $item->positif; ?> orang</h5>
          </div>
        </div>
      </div>
      <div class="col-sm-4 text-center text-success bg-dark">
        <div class="row p-5">
          <div class="col-3">
            <i class="far fa-smile fa-4x"></i>
          </div>
          <div class="col text-left">
            <h5><strong>TOTAL SEMBUH</strong></h5>
            <h5><?php echo $item->sembuh; ?> orang</h5>
          </div>
        </div>
      </div>
      <div class="col-sm-4 text-center text-danger bg-dark">
        <div class="row p-5">
          <div class="col-3">
            <i class="far fa-frown fa-4x"></i>
          </div>
          <div class="col text-left">
            <h5><strong>TOTAL MENINGGAL</strong></h5>
            <h5><?php echo $item->meninggal; ?> orang</h5>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>

  <script src="jquery-3.4.1.min.js"></script></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> 
  
  <script>
    $(document).ready(function(){
      $('#kecamatan').change(function(){
        var kecamatan_id = $(this).val();

        $.ajax({
          type : 'POST',
          url : 'data_kasus.php',
          data : 'kecamatan='+kecamatan_id,
          success: function(response){
            $('#tampil').html(response);
          }
        });
      })
    });

      mapboxgl.accessToken = 'pk.eyJ1IjoienVrZWVybmFuZGEiLCJhIjoiY2tmeWx0cDJuMmNiNjJ4bzhjbXNjM2NtZiJ9.uH4hshhusCg5AkBg_KgCLA';
      var petaku = new mapboxgl.Map({
          container: 'blokpeta',
          style: 'mapbox://styles/mapbox/streets-v11',
          center: [112.23344889101912, -7.55676746650353],
          zoom: 10
      });

      tampil();
      function tampil(){
        $.getJSON("json.php",{fungsi :"data_lokasi"}, function(result) {
            if (result.length != 0) {
                $.each(result, function (i, kolom) {
                    var bujur = kolom.bujur;
                    var lintang = kolom.lintang;
                    var positif = kolom.kasus_positif;
                    var sembuh = kolom.kasus_sembuh;
                    var meninggal = kolom.kasus_meninggal;
                    var nama = kolom.nama;
                    var jmls = kolom.jmls;
                    var id = kolom.id;
                    var coba = '\
                      <br>\
                      <div class="card">\
                        <div class="card-header alert-danger text-center p-1">\
                          <strong>Kecamatan<br>'+ nama +'</strong>\
                        </div>\
                        <div class="card-body p-0">\
                          <table class="table table-responsive-sm m-0">\
                            <tbody>\
                              <tr class="text-warning">\
                                <th><i class="far fa-sad-tear"></i> Kasus Positif</th>\
                                <th>'+ positif +'</th>\
                              </tr>\
                              <tr class="text-success">\
                                <th><i class="far fa-smile"></i> Kasus Sembuh</th>\
                                <th>'+ sembuh +'</th>\
                              </tr>\
                              <tr class="text-danger">\
                                <th><i class="far fa-frown"></i> Kasus Meninggal</th>\
                                <th>'+ meninggal +'</th>\
                              </tr>\
                            </tbody>\
                          </table>\
                        </div>\
                      </div>  \
                    ';
                    var el = document.createElement('div');
                    el.className = 'marker';

                    var pop = new mapboxgl.Popup({ offset: 25 }).setHTML(coba);
                    new mapboxgl.Marker(el).setLngLat([bujur, lintang]).setPopup(pop).addTo(petaku);
                });
            }else{
                alert("Data Tidak Di Temukan");
            }
        })
      }
        
      function jenispeta(jenis) {
        petaku.setStyle('mapbox://styles/mapbox/' + jenis)
      }   
  </script>

  <script type="text/javascript">
    function berhasil() {
        swal({
            title: "BERHASIL",
            text: "Data Kasus Telah ditambahkan",
            icon: "success",
            buttons: [false, "OK"],
          }).then(function() {
            return true;
          });
    }
    function salah() {
      swal ( "ERROR!" ,  "Username atau Password anda salah" ,  "error" );
    }
    
    document.querySelector(".logout").addEventListener('click', function(){
    //swal("Our First Alert");
    swal({
          title: "PERINGATAN",
          text: "Apakah anda yakin ingin keluar?",
          icon: "warning",
          buttons: [
            'TIDAK',
            'YA'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            window.location.href="logout.php";
          }
        })
  });
  </script>

<?php 
  include 'database.php';

  if ($_POST["submit"]) {
    
    $postif   = $_POST['positif'];
    $sembuh    = $_POST['sembuh'];
    $meninggal    = $_POST['meninggal'];
    $kecamatan = $_POST['kecamatan'];

    $query="UPDATE detail_lokasi SET kasus_positif='$postif', kasus_sembuh='$sembuh', kasus_meninggal='$meninggal' WHERE id_lokasi='$kecamatan'";
    $cek=mysqli_query($koneksi, $query);
    if($cek){
      echo "<script>berhasil();</script>";
    }
  }

  if($_GET['pesan'] == "gagal"){
    echo "<script>salah();</script>";
  }
?>

</body>
</html>