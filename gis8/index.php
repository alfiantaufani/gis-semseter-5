<html>
    <head>
        <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
        <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
        <style>
            .full-layar {
                width: 100%;
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
            }
            .layout {
                border: none;
                color: black;
                padding: 10px;
                text-align: center;
                display: inline-block;
                font-size: 14px;
                position: fixed;
                right: 10px;
                border-radius: 8px;
            }
               
        </style>
            }
    </head>
    <body>
        <div id='blokpeta' class="full-layar"></div>

        <!-- jquery -->
        <script src="jquery-3.4.1.min.js"></script>
        <script>
            mapboxgl.accessToken = 'pk.eyJ1IjoienVrZWVybmFuZGEiLCJhIjoiY2tmeWx0cDJuMmNiNjJ4bzhjbXNjM2NtZiJ9.uH4hshhusCg5AkBg_KgCLA';
            var petaku = new mapboxgl.Map({
                container: 'blokpeta',
                style: 'mapbox://styles/mapbox/satellite-v9',
                center: [112.23344889101912, -7.55676746650353],
                zoom: 13
            });

            tampildata();
            function tampildata() {
                $.getJSON("query.php" , {fungsi: "data_sekolah"}, function (result) {
                    if (result.lenght != 0) {
                        $.each(result, function(i, kolom) {
                            var bujur = kolom.bujur;
                            var lintang = kolom.lintang;
                            var nama = kolom.nama_sekolah;
                            var jmls = kolom.jmls;
                            var jmlg = kolom.jmlg;

                            var xyz = new mapboxgl.Popup({offset: 25 }).setHTML("<b>" + nama + "</b><br>Siswa : " + jmls + "<br>Guru : " + jmlg);
                            new mapboxgl.Marker().setLngLat([bujur, lintang]).setPopup(xyz).addTo(petaku);
                        });
                    }else{
                        alert("Data tidak tersedia");
                    }
                });
            }
            
        </script>
    </body>
</html>