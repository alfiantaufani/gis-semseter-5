<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GIS 6</title>

    <script src="https://use.fontawesome.com/bca161a0fd.js"></script>

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- mapbox -->
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />

    <!-- gl-direction -->
    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js"></script>
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.css" type="text/css" />

    <style>
        .full-width{
            width: 100%; 
            position : absolute;
            left : 0;
            top: 0;
            bottom : 0;
        }
        .a{
            position : fixed;
            margin : 20px;
        }
        .tombol {
            border: none;
            color: #000;
            padding: 10px;
            text-align: center;
            display: inline-block;
            font-size: 14px;
            position: fixed;
            left: 10px;
            border-radius: 8px;
        }
    </style>


</head>
<body>
    <div id="petaku" class="full-width"></div>
    <select style="top: 170px; width: 300px" class="tombol" id="bloktempat1">
    </select>

    <select style="top: 230px; width: 300px" class="tombol" id="bloktempat2">   
    </select>

    <button class="btn btn-success a mt-4" style="top : 270px;" onclick="hitungrute()"> Cari Peta</button><br>

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <!-- popper.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <!-- bootstrap.js -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://gis.crazycoding.info/uts/datauts.js"></script>
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiYWxmaWFudGF1ZmFuaSIsImEiOiJja2cyY2oxYTgwODExMnNxd3Iyc3ZuYzlxIn0.IOR4ClWmnwgYUW1VY5Gngw';
        var petaku = new mapboxgl.Map({
            container: 'petaku',
            style: 'mapbox://styles/mapbox/satellite-v9',
            //style: 'mapbox://styles/mapbox/streets-v11',
            center : [112.2320841, -7.5197630],//lokasi kampus
            zoom : 16,
        });

        petaku.addControl(
            new MapboxDirections({
                accessToken: mapboxgl.accessToken
            }),
            'top-left'
        );

        tampil(); // menjalankan function tampil
        function tampil() {
            var hasil = "";
            for(x in dataku){
                lokasi = dataku[x].lokasi;
                lintang = dataku[x].lintang;
                bujur = dataku[x].bujur;
                koordinat = bujur + "|" + lintang;

                hasil += "<option value='" + koordinat + "'>" + lokasi + "</option>"; 
            }            
            $("#bloktempat1").html(hasil);
            $("#bloktempat2").html(hasil);
        }

        function hitungrute() {
            var lokasi1 = $('#bloktempat1').val();
            var lokasi2 = $('#bloktempat2').val();
            if (lokasi1 == "" || lokasi2 == "") {
                alert("Form lokasi tidak boleh kosong");
                return;
            }
            if (lokasi1 == lokasi2) {
                alert("Lokasi tidak boleh sama");
                return;
            }

            data1 = lokasi1.split("|");
            data2 = lokasi2.split("|");
            bujur1 = data1[0];
            lintang1 = data1[1];
            bujur2 = data2[0];
            lintang2 = data2[1];
            var urlapi = "https://api.mapbox.com/directions/v5/mapbox/driving/" + bujur1 + "," + lintang1 + ";" + bujur2 + "," + lintang2 + "?steps=true&geometries=geojson&access_token=" + mapboxgl.accessToken;
            $.ajax({
                method : 'GET',
                url : urlapi,
            }).done(function(data) {
                var rute = data.routes[0].geometry;
                petaku.addLayer({
                    id : 'route',
                    type : 'line',
                    source : {
                        type : geojson,
                        data : {
                            type : 'Feature',
                            geometry : rute
                        }
                    },
                    paint : {
                        'line-width' : 8,
                        'line-color' : 'red'
                    },
                });
            });
        }

        /*
        
        */

    </script>
    
</body>
</html>