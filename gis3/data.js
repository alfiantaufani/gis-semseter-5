var dataku = {
    'kumpulan' : [
        {
            'tipe': 'Feature',
            'properties': 
            {
                'judul': 'Unwaha',
                'deskripsi':'Universitas KH. A. Wahab Hasbullah'
            },
            'geometry': 
            {
                'coordinates': [112.2319875404811, -7.520193780700623],
                'type': 'Point'
            }
        },

        {
            'tipe': 'Feature',
            'properties': 
            {
                'judul': 'My Home',
                'deskripsi':'Rumahku'
            },
            'geometry': 
            {
                'coordinates': [112.25895161842908, -7.51002869518841],
                'type': 'Point'
            }
        },

        {
            'tipe': 'Feature',
            'properties': 
            {
                'judul': 'Toko AA Jaya Mix on the mix',
                'deskripsi':'Bisa di atur'
            },
            'geometry': 
            {
                'coordinates': [112.23383901306613, -7.5183099367177135],
                'type': 'Point'
            }
        },

        {
            'tipe': 'Feature',
            'properties': 
            {
                'judul': 'POM PERTAMINA Tambakberas Jombang',
                'deskripsi':'Ngepom o lur'
            },
            'geometry': 
            {
                'coordinates': [112.23293634084104, -7.521409351344062],
                'type': 'Point'
            }
        },

        {
            'tipe': 'Feature',
            'properties': 
            {
                'judul': 'Talia Cape',
                'deskripsi':'Tempat Yoklo Yoklo'
            },
            'geometry': 
            {
                'coordinates': [112.22157823210398, -7.5493168578052945],
                'type': 'Point'
            }
        },
    ]
}