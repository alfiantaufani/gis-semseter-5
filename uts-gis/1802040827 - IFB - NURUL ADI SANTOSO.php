<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UTS GIS | @NA</title>
    <script src="https://use.fontawesome.com/bca161a0fd.js"></script>
    
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />


    <style>
        .full-width{
            width: 100%; 
            position : absolute;
            left : 0;
            top: 0;
            bottom : 0;
        }
    </style>


</head>
<body>

    
    <div id="petaku" class="full-width"></div>
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://gis.crazycoding.info/uts/datauts.js"></script>
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiYWxmaWFudGF1ZmFuaSIsImEiOiJja2cyY2oxYTgwODExMnNxd3Iyc3ZuYzlxIn0.IOR4ClWmnwgYUW1VY5Gngw';
        var petaku = new mapboxgl.Map({
            container: 'petaku',
            style: 'mapbox://styles/mapbox/satellite-v9',
            //style: 'mapbox://styles/mapbox/streets-v11',
            center : [112.23920860705323, -7.546982935115636],//lokasi kampus
            zoom : 15,
        });

        //new mapboxgl.Marker().setLngLat([112.2320841, -7.5197630]).addTo(petaku)

        
        tampil(); // menjalankan function tampil

        function tampil() {
            var x; // membuat variable x
            for(x in dataku) {                
                lokasi = dataku[x].lokasi; // mengambil data lokasi
                lintang = dataku[x].lintang; // mengambil data lintang
                bujur = dataku[x].bujur; // mengambil data bujur

                koordinat = [bujur, lintang]; // mengabungkan antara bujur dan lintang

                new mapboxgl.Marker().setLngLat(koordinat).addTo(petaku); // membuat marker sesuai koordinat
                //alert(koordinat);
            }
        }
        
      
    </script>
    
</body>
</html>