<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GIS 5</title>
    <script src="https://use.fontawesome.com/bca161a0fd.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />

    <style>
        .full-width{
            width: 100%; 
            position : absolute;
            left : 0;
            top: 0;
            bottom : 0;
        }
        .a{
            position : fixed;
            margin : 20px;
        }
    </style>

    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css" type="text/css"/>

</head>
<body>

    
    <div id="petaku" class="full-width"></div>
    
    <button class="btn btn-success a mt-4" style="top : 0px;" onclick="cek()"> Deteksi Lokasi</button><br>

    <button class="btn btn-warning a mt-4" style="top : 50px;" onclick="jenispeta('satellite-v9')"> Peta Satelit</button><br>

    <button class="btn btn-info a mt-4" style="top : 100px;" onclick="jenispeta('streets-v11')"> Peta Street</button>
            
    <button class="btn btn-danger a mt-4" style="top : 150px;" onclick="jenispeta('light-v10')"> Peta Light</button>

    <div class="card mb-3" style="max-width: 18rem; top: 200px; margin-left: 20px;">
        <div class="card-header text-secondary"><h5><i class="fa fa-info-circle"></i> INFORMASI</h5></div>
        <div class="card-body text-info">
            <h6 class="card-title">Koordinat terbaru </h6>
            <p id="blokkoodinat" class="card-text"></p>
            <a href="#" class="btn btn-info">Go somewhere</a>
        </div>
    </div>





    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiYWxmaWFudGF1ZmFuaSIsImEiOiJja2cyY2oxYTgwODExMnNxd3Iyc3ZuYzlxIn0.IOR4ClWmnwgYUW1VY5Gngw';
        var petaku = new mapboxgl.Map({
            container: 'petaku',
            //style: 'mapbox://styles/mapbox/satellite-v9',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [112.23344889101912, -7.55676746650353],
            zoom : 10,
        });
        var tanda = new mapboxgl.Marker({draggable: true}).setLngLat([112.23344889101912, -7.55676746650353]).addTo(petaku);

        petaku.addControl(
            new mapboxgl.GeolocateControl({
                positionOptions: {
                    enableHighAccuracy: true
                },
                trackUserLocation: true
            })
        );

        petaku.addControl(
            new MapboxGeocoder({
                accessToken: mapboxgl.accessToken,
                mapboxgl: mapboxgl
            })
        );

        function cek(){
            if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(sukses, gagal);
            }else{
                alert("Maaf Browser tidak mendukung Geolocation!");
            }
        }

        function sukses(position) {
            var lintang = position.coords.latitude;
            var bujur = position.coords.longitude;
            alert("Posisi di temukan di \n Lintang : " + lintang + "\n Bujur : " + bujur);
            petaku.setCenter([bujur, lintang]);
            tanda.setLngLat([bujur, lintang]);
        }

        function gagal(error) {
            switch(error.code){
                case error.PERMISSION_DENIED:
                    alert("AKASES DI BLOKIR");
                    break;
                case error.POSITION_UNAVAILABLE:
                    alert("LOKASI TIDAK DI TEMUKAN");
                    break;
                case error.TIMEOUT:
                    alert("TIME OUT");
                    break;
                case error.UNKNOWN_ERROR:
                    alert("MASALAH TIDAK TERDETEKSI");
                    break;
            }
        }

        function jenispeta(jenis) {
            petaku.setStyle('mapbox://styles/mapbox/' + jenis)
            
        }

        function tampil() {
            var koordinat = tanda.getLngLat();
            var lintang = koordinat.lat; // mengambil lintang
            var bujur = koordinat.lng; // mengambil bujur
            var isi = "Lintang : " + lintang + "<br>Bujur : " + bujur;
            document.getElementById("blokkoodinat").innerHTML = isi;
        }
        tanda.on("dragend", tampil);

        

    </script>
    
</body>
</html>